#include <iostream>
#include <fstream>
#include <filesystem>
#include <vector>
#include <string>
#include <sstream>
#include <memory>
#include <cassert>

#include <mpi.h>
#include <openssl/sha.h>

#define FIXED_WORD_SIZE 1
const constexpr size_t word_buffer_length = 8 + 1;

bool is_same_hash(const unsigned char* hash_1, const unsigned char* hash_2)
{
    return std::get<0>(std::mismatch(hash_1, hash_1 + SHA512_DIGEST_LENGTH, hash_2)) == (hash_1 + SHA512_DIGEST_LENGTH);
}

unsigned char char_to_uchar(char c)
{
    if(c >= '0' && c <= '9')
        return c - '0';
    else if(c >= 'a' && c <= 'f')
        return 10 +  c - 'a';
    else if(c >= 'A' && c <= 'F')
        return 10 +  c - 'A';
    else
        throw std::runtime_error("Invalid char");
}

void convert_hash_string_to_uchar(const std::string& hash_string, unsigned char* hash)
{
    assert(hash_string.size() == SHA512_DIGEST_LENGTH * 2);
    for(size_t i = 0; i < SHA512_DIGEST_LENGTH; ++i)
        hash[i] = char_to_uchar(hash_string[i * 2]) * 16 + char_to_uchar(hash_string[i * 2 + 1]);
}

std::string hash_to_string(unsigned char* hash)
{
    std::stringstream ss;
    for(int i = 0; i < SHA512_DIGEST_LENGTH; i++)
    {
        ss << std::hex << std::setw(2) << std::setfill('0') << (int)hash[i];
    }
    return ss.str();
}

std::vector<std::string> read_dict(const std::filesystem::path& path, size_t words_to_read)
{
    std::ifstream if_dict(path);
    std::vector<std::string> words(words_to_read);
    size_t w = 0;

    for(w = 0; w < words_to_read; ++w)
    {
        if(!if_dict.good())
        {
            --w;
            break;
        }
        std::getline(if_dict, words[w]);
#if FIXED_WORD_SIZE
        if((words[w].size() + 1) != word_buffer_length)
            --w;
#endif
    }
    if(w < words_to_read)
    {
        words.resize(w + 1);
        std::cerr << "The dictionary contained less words than requested." << std::endl;
    }
    std::cout << "Loaded " << words.size()  << " words." << std::endl;

    return words;
}

std::string pack_data(const std::vector<std::string>& words, size_t start_index, size_t last_index)
{
    std::stringstream sstream;
    for(size_t i = start_index; i < last_index; ++i)
        sstream << words[i] << " ";
    sstream << words[last_index];
    return sstream.str(); // Note that this is slow as it copies the buffer, but that's good enough for this exercise
}

std::string look_for_hash(const unsigned char* hash_to_look_for, const std::vector<std::string>& words, size_t& hash_count)
{
    hash_count = 0;

    unsigned char hash[SHA512_DIGEST_LENGTH];
    for(const std::string& word: words)
    {
        ++hash_count;
        SHA512((const unsigned char*)word.c_str(), word.size(), hash);
        if(is_same_hash(hash, hash_to_look_for))
            return word;
    }
    return {};
}

char* allocate_char_buffer(size_t size)
{
    // The idiomatic way of allocating in cpp is to use unique_ptr but we make it look
    // more like C/Java for people not familiar with c++
    // Do no forget to call free_char_buffer!
    // std::unique_ptr<char[]> data = std::make_unique<char[]>(num_bytes_to_receive);
    return new char[size];
}

void free_char_buffer(char* buffer)
{
    delete[] buffer;
    buffer = nullptr;
}

std::vector<std::string> string_buffer_to_string_vector(const char* buffer, size_t buffer_size)
{
    std::stringstream sstream;
    std::vector<std::string> words;
    std::string word;
    sstream.write(buffer, buffer_size);
    while(sstream.good())
    {
        words.emplace_back();
        sstream >> words.back();
    }
    return words;
}

/*
 * You can use these tags for your MPI messages
 */
enum MPI_tags : int {
    WORD,
    LAST_WORD,
    RESULT
};

int send_words_to_nodes(const std::vector<std::string>& words, int number_nodes)
{
    /*
     * TODO You must send the words to the cluster compute nodes. There are many
     * alternatives more or less efficient.  For instance, you can try to send
     * them one by one.
     */
    for(const std::string& word: words)
    {
        // To send words one at a time, you can uncomment the following,
        // fill-in the missing parameters, and complete the code. Do not forget
        // that you will have to tell the receiving node that you are done
        // sending things; you can use the MPI_tags.

        /*
        int err = MPI_Send(word.c_str(), word_buffer_length, MPI_BYTE, , WORD, MPI_COMM_WORLD);
        if(err != 0)
        {
            std::cerr << "Error sending words" << std::endl;
            return err;
        }
        */
    }

    return 0;
}

int receive_words(std::vector<std::string>& words)
{
    /*
     * TODO Now you need to receive the words on each compute node.  The
     * structure of this function will depend on how you chose to send the
     * words. Keep in mind that you must have a way to tell when you have
     * received everything.
     *
     * You can allocate the receive buffers using allocate_char_buffer()
     * and free them using free_char_buffer().
     */
    MPI_Status status;

    // To receive words one by one, you can uncomment the code below.
    // The words will be added one by one to the "words" std::vector of std::string;
    // in the main you will be able to process these words.
    // Remember that you will have to find a way to terminate the while loop.
    /*
    bool expect_another_word = true;
    while(expect_another_word)
    {
        int num_bytes_to_receive = word_buffer_length * 1;
        char* buffer = allocate_char_buffer(num_bytes_to_receive);
        err = MPI_Recv(buffer, num_bytes_to_receive, MPI_BYTE, 0, WORD, MPI_COMM_WORLD, &status);
        if(err != 0)
        {
            std::cerr << "Error receiving words" << std::endl;
            return err;
        }

        words.emplace_back(buffer); // Because buffer is null-terminated
        free_char_buffer(buffer);

        MPI_tags tag = (MPI_tags) status.MPI_TAG; (void) tag;
    }
    */

    return 0;
}

int send_back_word(const std::string& word)
{
    /*
     * TODO: in this function you must send back to master the word you have found
     * or an empty string (meaning a string containing only the terminating character)
     * to the master node.
     *
     * Use the send_words_to_nodes to get inspiration!
     */
    return 0;
}

int receive_found_word(std::string& word, int number_nodes)
{
    /*
     * TODO: Now you must wait for the results of the nodes and see if one of
     * them found the word you provided the hash of.
     *
     * HINT: you can receive an MPI message from any node by specifying
     * MPI_ANY_SOURCE as id.
     */

    bool found_word = false;

    /*
    for(int node_id = 1; node_id <= number_nodes; ++node_id)
    {
        int num_bytes_to_receive = word_buffer_length * 1;
        char* buffer = allocate_char_buffer(num_bytes_to_receive);
        err = MPI_Recv(buffer, num_bytes_to_receive, MPI_BYTE, , , MPI_COMM_WORLD, &status);
        if(err != 0)
        {
            std::cerr << "Error receiving found word" << std::endl;
            return err;
        }
        word = std::string(buffer);
        free_char_buffer(buffer);

        if(word.size() > 0)
        {
            std::cout << "Node " << status.MPI_SOURCE << " found the word \"" << word << "\"" << std::endl;
            found_word = true;
        }
    }
    */

    if(!found_word)
        std::cout << "Nobody found the word :'(" << std::endl;

    return 0;
}

int main(int argc, char* argv[])
{
    int id, err, total_number_nodes;

    if(argc != 4)
    {
        std::cerr << "Usage: " << argv[0] << " dictionary_filename number_words_to_load hash_to_look_for" << std::endl;
        return EXIT_FAILURE;
    }

    std::filesystem::path dict_path(argv[1]);
    size_t n_words = std::stoull(argv[2]);
    if(!std::filesystem::is_regular_file(std::filesystem::status(dict_path)))
    {
        std::cerr << "Invalid dictionary file." << std::endl;
        return EXIT_FAILURE;
    }

    /*
     * Set things up
     */
    err = MPI_Init(&argc, &argv);
    if(err != MPI_SUCCESS)
    {
        std::cerr << "Failed MPI init" << std::endl;
        return EXIT_FAILURE;
    }

    err = MPI_Comm_size(MPI_COMM_WORLD, &total_number_nodes);
    if(err != MPI_SUCCESS)
    {
        std::cerr << "Failed MPI call" << std::endl;
        return EXIT_FAILURE;
    }
    err = MPI_Comm_rank(MPI_COMM_WORLD, &id);
    if(err != MPI_SUCCESS)
    {
        std::cerr << "Failed MPI call" << std::endl;
        return EXIT_FAILURE;
    }

    // The arguments are available to everybody
    unsigned char hash_to_look_for[SHA512_DIGEST_LENGTH];
    convert_hash_string_to_uchar(argv[3], hash_to_look_for);

    /*
     * Main work code
     */
    if(id == 0)
    {
        // if we are on the master node
        size_t number_nodes = total_number_nodes - 1;
        std::cout << "Message from master:\n\tWorld size : " << total_number_nodes << std::endl;

        // Load the dictionary
        std::vector<std::string> words = read_dict(dict_path, n_words);

        err = send_words_to_nodes(words, number_nodes);

        std::string word;
        err = receive_found_word(word, number_nodes);
    }

    if(id != 0)
    {
        // Receive the word buffer
        std::vector<std::string> words;
        err = receive_words(words);

        // Look for the hash
        size_t num_hashes_computed = 0;
        std::string word = look_for_hash(hash_to_look_for, words, num_hashes_computed);
        std::cout << "Node " << id << " computed " << num_hashes_computed << " hashes." << std::endl;

        // Send back the word to master (send empty word if we found nothing)
        err = send_back_word(word);
    }

    /*
     * Close up shop
     */
    err = MPI_Finalize();
    if(err != MPI_SUCCESS)
    {
        std::cerr << "Failed MPI finalize" << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

